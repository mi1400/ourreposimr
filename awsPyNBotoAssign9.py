import boto3
import botocore
###### aws configure get region
###### aws configure set region us-east-2
from botocore.exceptions import ClientError
import json
#import urllib2     #malware

#NOTE: this is CLONE of Assignment-7 .. to see all the removed comments see #7

# aws sts get-caller-identity
# aws configure get region
# aws configure set region us-east-2
# client = boto3.client('ec2',region_name='us-east-2')
# response = client.describe_instances()
# print(response)

#========================================================================================================================================================
#=================   ASSIGNMENT-8 code-base   (commenting and not running as already existing and not deleted yet from AWS as i usally do)  =============
#========================================================================================================================================================

# session = boto3.session.Session(profile_name='default')
# iam = session.client('iam')
# path='/'
# role_name='ec2_DDB_Role_IMR_08'
# description='BOTO3 ec2 test role IMR'

# trust_policy={
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       #"Sid": "",                           #???????????? StatementIDs (SID)
#       "Effect": "Allow",
#       "Principal": {                        #Principal – In identity-based policies (IAM policies), the user that the policy is attached to is the implicit principal. For resource-based policies, you specify the user, account, service, or other entity that you want to receive permissions (applies to resource-based policies only). DynamoDB doesn't support resource-based policies.
#                                             #tell AWS that EC2 is allowed to use this role by listing the EC2 service in the role's trust policy (they're being very polite about this; they'd never just assume that you wanted EC2 to access a role without your explicit say so.)    ||| Go into IAM > Policies > Create Policy > Policy Generator. Build your policy there and you can't go wrong. ||| https://www.reddit.com/r/aws/comments/4zq5sh/help_with_elastic_beanstalk_dynamodb_tutorial/
#         "Service": "ec2.amazonaws.com"      #not usable by lambda in this scenario
#       },
#       "Action": "sts:AssumeRole"
#     }
#   ]
# }
# #^^^^^^^^^^^^^vvvvvvvvvvvvvvvvv
# response = iam.create_role(
#     Path=path,
#     RoleName=role_name,
#     AssumeRolePolicyDocument=json.dumps(trust_policy),    #https://forums.aws.amazon.com/thread.jspa?messageID=831798&tstart=0        #https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3
#     Description=description,
#     MaxSessionDuration=1800 #<seconds -- 3600(1hr) by default
# )

# #======================================================================================================================================================
# #dynamodb managed policy

# managed_policy = {                  #original: https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3
#     "Version": "2012-10-17",
#     "Statement": [
#         # {
#         #     "Sid": "ListAndDescribe", #"S3ReadOnly",
#         #     "Effect": "Deny", #"Allow"                            #Changing to DENY to make sense in 2 blocks
#         #     "Action": [
#         #         "dynamodb:List*", #"s3:Get*",
#         #         "dynamodb:DescribeReservedCapacity*", #"s3:List*"
#         #         "dynamodb:DescribeLimits",
#         #         "dynamodb:DescribeTimeToLive"
#         #     ],
#         #     "Resource": [
#         #         "*"
#         #     ]
#         # },
#         {
#             "Sid": "Ec2FullAccess", #"Ec2FullAccess", #SpecificTable #"DDB_Movies_IMR"
#             "Effect": "Allow",
#             "Action": "dynamodb:*", #https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_dynamodb_specific-table.html
#             "Resource": "arn:aws:dynamodb:us-east-2:832922473570:table/DDB_Movies_IMR"      #   "*" also WORK!!!        #"arn:aws:dynamodb:us-west-2:123456789012:table/Books"      #The ARN value specified in the Resource identifies a table in a specific Region.
#         }
#     ]
# }
# #^^^^^^^^^^^^^vvvvvvvvvvvvvvvvv
# response = iam.create_policy(
#   PolicyName='testEc2PolicyIMR_02', #BOTO3TestEc2PolicyIMR                                        #botocore.errorfactory.MalformedPolicyDocumentException: An error occurred (MalformedPolicyDocument) when calling the CreatePolicy operation: Statement IDs (SID) must be alpha-numeric. Check that your input satisfies the regular expression [0-9A-Za-z]*
#   PolicyDocument=json.dumps(managed_policy)
# )

# #======================================================================================================================================================
# iam.attach_role_policy(
#     #PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess'                    #https://www.programcreek.com/python/example/97939/boto3.session
#     PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess',                    #'arn:aws:iam::${account_id}:policy/testEc2PolicyIMR' #https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3
#                                                                                 #VSCODE-ERROR: botocore.errorfactory.InvalidInputException: An error occurred (InvalidInput) when calling the AttachRolePolicy operation: ARN arn:aws:iam::${account_id}:policy/testEc2PolicyIMR is not valid.
#     RoleName='ec2_DDB_Role_IMR_08'
# )

#======================================================================================================================================================

#NOT OVER .. FINAL STEP... ELSE "Instance Profile ARNs" will remain MISSING/NULL in IAM > ROLE > 3rd-ROW "Instance Profile ARNs"
# IT has remained NULL for all except now i fixed with CLI code at bottom of this section
#ec2_DDB_Role_IMR
#ec2_DDB_Role_IMR_02
#ec2_DDB_Role_IMR_03
#ec2_DDB_Role_IMR_04
#ec2_DDB_Role_IMR_05
#ec2_DDB_Role_IMR_06
#ec2_DDB_Role_IMR_07
#ec2_DDB_Role_IMR_08      <<<<<<<<<<<<<<< FIXED
#ec2_DDB_Role_IMR_10
#ec2_DDB_Role_IMR_11

#********** FIXER ***********
#CLI way
#========
# aws iam create-instance-profile --instance-profile-name MyExistingRole
# aws iam add-role-to-instance-profile --instance-profile-name MyExistingRole --role-name MyExistingRole

# aws iam create-instance-profile --instance-profile-name ec2_DDB_Role_IMR_08
# aws iam add-role-to-instance-profile --instance-profile-name ec2_DDB_Role_IMR_08 --role-name ec2_DDB_Role_IMR_08

# aws iam create-instance-profile --instance-profile-name ec2_DDB_Role_IMR_11                                         #https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.create_instance_profile
# aws iam add-role-to-instance-profile --instance-profile-name ec2_DDB_Role_IMR_11 --role-name ec2_DDB_Role_IMR_11    #https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.add_role_to_instance_profile

#Boto way
#========
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.create_instance_profile
# response = client.create_instance_profile(
#     InstanceProfileName='ec2_DDB_Role_IMR_11',    #'Webserver',
# )
# print(response)

#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.add_role_to_instance_profile
# response = client.add_role_to_instance_profile(
#     InstanceProfileName='ec2_DDB_Role_IMR_11',    #'Webserver',
#     RoleName='ec2_DDB_Role_IMR_11',               #'S3Access',
# )
# print(response)

#========================================================================================================================================================
#========================================================           ASSIGNMENT-7 code-base           ====================================================
#========================================================================================================================================================
#EXCEPTION HANDLING WAY----
#==========================
ec2 = boto3.client('ec2', region_name='us-east-2')

response = ec2.describe_vpcs()
v_vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', 'vpc-a360a8c8')           #???????????? should have called by NAME .. e.g. PRODUCTION_VPN
#v_security_group_id = None

try:
    response = ec2.create_security_group(GroupName='SGforInstanceIMR',
                                         Description='My new SG for Instance to access from home',
                                         VpcId=v_vpc_id)                                               #obselete: Tried ... "Name='SGforInstanceIMR')" #Unknown parameter in input: "Name", must be one of: Description, GroupName, VpcId, DryRun
    v_security_group_id = response['GroupId']
    print('Security Group Created %s in vpc %s.' % (v_security_group_id, v_vpc_id))

    # import urllib2            #malware
    # def get_public_ip():
    # ext_ip = urllib2.urlopen("http://curlmyip.com").read()
    # return ext_ip.strip()

    data2 = ec2.authorize_security_group_ingress(
        GroupId=v_security_group_id,
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 80,
             'ToPort': 80,
             'IpRanges': [{'CidrIp': '70.31.63.90/32'}]}, #70.31.63.90/32        0.0.0.0/0          ????? How implement MY-IP
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '70.31.63.90/32'}]} #70.31.63.90/32         0.0.0.0/0          ????? How implement MY-IP
        ])
    print('Ingress Successfully Set %s' % data2)
except ClientError as e:
    print(e)

#==================================================================================================================================================================

v_userData = """
                Content-Type: multipart/mixed; boundary="//"
                MIME-Version: 1.0

                --//
                Content-Type: text/cloud-config; charset="us-ascii"
                MIME-Version: 1.0
                Content-Transfer-Encoding: 7bit
                Content-Disposition: attachment; filename="cloud-config.txt"

                #cloud-config
                cloud_final_modules:
                - [scripts-user, always]

                --//
                Content-Type: text/x-shellscript; charset="us-ascii"
                MIME-Version: 1.0
                Content-Transfer-Encoding: 7bit
                Content-Disposition: attachment; filename="userdata.txt"

                #!/bin/bash
                /bin/echo "Hello World" >> /tmp/testfile.txt
                --//
             """

v_userData = """
                #!/bin/bash
                sudo yum update -y
                sudo yum install -y httpd
                sudo echo '<h1>Hello World</h1>' > /var/www/html/index.html
                sudo systemctl start httpd
                sudo systemctl enable httpd
             """

v_userData = """
                #!/bin/bash
                sudo yum update -y
                sudo yum install -y httpd
                echo "<h1>Hello World</h1>" | sudo tee /var/www/html/index.html
                sudo systemctl start httpd
                sudo systemctl enable httpd
             """

ec2b = boto3.resource('ec2', region_name='us-east-2')
instance = ec2b.create_instances(
    ImageId='ami-0f7919c33c90f5b58', #IMR
    InstanceType='t2.micro',                                  #'t1.micro'|'t2.nano'|'t2.micro'|'t2.small'|'t2.medium'|'t2.large'|'t2.xlarge'|'t2.2xlarge'|'t3.nano'|'t3.micro'|'t3.small'|'t3.medium'|'t3.large'|'t3.xlarge'|'t3.2xlarge'|'t3a.nano'|'t3a.micro'|'t3a.small'|'t3a.medium'|'t3a.large'|'t3a.xlarge'|'t3a.2xlarge'|'m1.small'|'m1.medium'|'m1.large'|'m1.xlarge'|'m3.medium'|'m3.large'|'m3.xlarge'|'m3.2xlarge'|'m4.large'|'m4.xlarge'|'m4.2xlarge'|'m4.4xlarge'|'m4.10xlarge'|'m4.16xlarge'|'m2.xlarge'|'m2.2xlarge'|'m2.4xlarge'|'cr1.8xlarge'|'r3.large'|'r3.xlarge'|'r3.2xlarge'|'r3.4xlarge'|'r3.8xlarge'|'r4.large'|'r4.xlarge'|'r4.2xlarge'|'r4.4xlarge'|'r4.8xlarge'|'r4.16xlarge'|'r5.large'|'r5.xlarge'|'r5.2xlarge'|'r5.4xlarge'|'r5.8xlarge'|'r5.12xlarge'|'r5.16xlarge'|'r5.24xlarge'|'r5.metal'|'r5a.large'|'r5a.xlarge'|'r5a.2xlarge'|'r5a.4xlarge'|'r5a.8xlarge'|'r5a.12xlarge'|'r5a.16xlarge'|'r5a.24xlarge'|'r5d.large'|'r5d.xlarge'|'r5d.2xlarge'|'r5d.4xlarge'|'r5d.8xlarge'|'r5d.12xlarge'|'r5d.16xlarge'|'r5d.24xlarge'|'r5d.metal'|'r5ad.large'|'r5ad.xlarge'|'r5ad.2xlarge'|'r5ad.4xlarge'|'r5ad.8xlarge'|'r5ad.12xlarge'|'r5ad.16xlarge'|'r5ad.24xlarge'|'x1.16xlarge'|'x1.32xlarge'|'x1e.xlarge'|'x1e.2xlarge'|'x1e.4xlarge'|'x1e.8xlarge'|'x1e.16xlarge'|'x1e.32xlarge'|'i2.xlarge'|'i2.2xlarge'|'i2.4xlarge'|'i2.8xlarge'|'i3.large'|'i3.xlarge'|'i3.2xlarge'|'i3.4xlarge'|'i3.8xlarge'|'i3.16xlarge'|'i3.metal'|'i3en.large'|'i3en.xlarge'|'i3en.2xlarge'|'i3en.3xlarge'|'i3en.6xlarge'|'i3en.12xlarge'|'i3en.24xlarge'|'i3en.metal'|'hi1.4xlarge'|'hs1.8xlarge'|'c1.medium'|'c1.xlarge'|'c3.large'|'c3.xlarge'|'c3.2xlarge'|'c3.4xlarge'|'c3.8xlarge'|'c4.large'|'c4.xlarge'|'c4.2xlarge'|'c4.4xlarge'|'c4.8xlarge'|'c5.large'|'c5.xlarge'|'c5.2xlarge'|'c5.4xlarge'|'c5.9xlarge'|'c5.12xlarge'|'c5.18xlarge'|'c5.24xlarge'|'c5.metal'|'c5d.large'|'c5d.xlarge'|'c5d.2xlarge'|'c5d.4xlarge'|'c5d.9xlarge'|'c5d.12xlarge'|'c5d.18xlarge'|'c5d.24xlarge'|'c5d.metal'|'c5n.large'|'c5n.xlarge'|'c5n.2xlarge'|'c5n.4xlarge'|'c5n.9xlarge'|'c5n.18xlarge'|'cc1.4xlarge'|'cc2.8xlarge'|'g2.2xlarge'|'g2.8xlarge'|'g3.4xlarge'|'g3.8xlarge'|'g3.16xlarge'|'g3s.xlarge'|'g4dn.xlarge'|'g4dn.2xlarge'|'g4dn.4xlarge'|'g4dn.8xlarge'|'g4dn.12xlarge'|'g4dn.16xlarge'|'cg1.4xlarge'|'p2.xlarge'|'p2.8xlarge'|'p2.16xlarge'|'p3.2xlarge'|'p3.8xlarge'|'p3.16xlarge'|'p3dn.24xlarge'|'d2.xlarge'|'d2.2xlarge'|'d2.4xlarge'|'d2.8xlarge'|'f1.2xlarge'|'f1.4xlarge'|'f1.16xlarge'|'m5.large'|'m5.xlarge'|'m5.2xlarge'|'m5.4xlarge'|'m5.8xlarge'|'m5.12xlarge'|'m5.16xlarge'|'m5.24xlarge'|'m5.metal'|'m5a.large'|'m5a.xlarge'|'m5a.2xlarge'|'m5a.4xlarge'|'m5a.8xlarge'|'m5a.12xlarge'|'m5a.16xlarge'|'m5a.24xlarge'|'m5d.large'|'m5d.xlarge'|'m5d.2xlarge'|'m5d.4xlarge'|'m5d.8xlarge'|'m5d.12xlarge'|'m5d.16xlarge'|'m5d.24xlarge'|'m5d.metal'|'m5ad.large'|'m5ad.xlarge'|'m5ad.2xlarge'|'m5ad.4xlarge'|'m5ad.8xlarge'|'m5ad.12xlarge'|'m5ad.16xlarge'|'m5ad.24xlarge'|'h1.2xlarge'|'h1.4xlarge'|'h1.8xlarge'|'h1.16xlarge'|'z1d.large'|'z1d.xlarge'|'z1d.2xlarge'|'z1d.3xlarge'|'z1d.6xlarge'|'z1d.12xlarge'|'z1d.metal'|'u-6tb1.metal'|'u-9tb1.metal'|'u-12tb1.metal'|'u-18tb1.metal'|'u-24tb1.metal'|'a1.medium'|'a1.large'|'a1.xlarge'|'a1.2xlarge'|'a1.4xlarge'|'a1.metal'|'m5dn.large'|'m5dn.xlarge'|'m5dn.2xlarge'|'m5dn.4xlarge'|'m5dn.8xlarge'|'m5dn.12xlarge'|'m5dn.16xlarge'|'m5dn.24xlarge'|'m5n.large'|'m5n.xlarge'|'m5n.2xlarge'|'m5n.4xlarge'|'m5n.8xlarge'|'m5n.12xlarge'|'m5n.16xlarge'|'m5n.24xlarge'|'r5dn.large'|'r5dn.xlarge'|'r5dn.2xlarge'|'r5dn.4xlarge'|'r5dn.8xlarge'|'r5dn.12xlarge'|'r5dn.16xlarge'|'r5dn.24xlarge'|'r5n.large'|'r5n.xlarge'|'r5n.2xlarge'|'r5n.4xlarge'|'r5n.8xlarge'|'r5n.12xlarge'|'r5n.16xlarge'|'r5n.24xlarge'|'inf1.xlarge'|'inf1.2xlarge'|'inf1.6xlarge'|'inf1.24xlarge',
    KeyName='Key_Test2_IMR',
    MaxCount=1, #123,
    MinCount=1, #123,
    # SecurityGroups=[                                        # Network interfaces and an instance-level security groups may not be specified on the same request
    #      'SGforInstanceIMR', #'string',
    #  ],
    # SubnetId='subnet-0abd08a11c2f5fd58', #'string',         # Network interfaces and an instance-level subnet ID may not be specified on the same request
    UserData=v_userData, #'string',
    DisableApiTermination=False, #True|False,                           # ?????????
    # DryRun=True|False,
    IamInstanceProfile={                    # == ROLE
        'Arn': 'arn:aws:iam::832922473570:instance-profile/ec2_DDB_Role_IMR_11'   #'arn:aws:iam::832922473570:policy/testEc2PolicyIMR_02'   #'arn:aws:iam::832922473570:role/ec2_DDB_Role_IMR_08'    #"Has" now become Assignment9 giving here... find and copy ARN from IAM console >>> "arn:aws:iam::832922473570:role/ec2_DDB_Role_IMR_08" (<ARN from Assignment8 ie. last ROLE i created through this code) ### Can remove this through EC2 > Instance > Rightclick > Edit Profile something (around TERMINATE menue options/rightclick-popup)
                                                                                  #An error occurred (InvalidParameterValue) when calling the RunInstances operation: Value (arn:aws:iam::832922473570:policy/testEc2PolicyIMR_02) for parameter iamInstanceProfile.arn is invalid. Invalid IAM Instance Profile ARN        
        #'Name': 'ec2_DDB_Role_IMR_08'       #DO NOT USE BOTH                     #The parameter 'iamInstanceProfile.name' may not be used in combination with 'iamInstanceProfile.arn'
    },
    InstanceInitiatedShutdownBehavior='stop', #'stop'|'terminate',
    NetworkInterfaces=[ #-------------------------------------------------------------------------------------
        {
            'AssociatePublicIpAddress': True, #True|False,
            'DeleteOnTermination': True, #True|False,
            'Description': 'Boto Created Instance IMR',      #?????????????
            'DeviceIndex': 0, #123,                                                          # The associatePublicIPAddress parameter can only be specified for the network interface with DeviceIndex 0.
            'Groups': [              #<<< SG                                                  # GroupId (string) -- The ID of the security group. You must specify either the security group ID or the security group name in the request. For security groups in a nondefault VPC, you must specify the security group ID.          Groups == SecurityGroups -- IMR
                 v_security_group_id #(<WORKS) #response['GroupId']#(<WORKS)                  # The security group 'sgforinstanceimr' does not exist in VPC 'vpc-a360a8c8' ..... MUST SET REGION ---AND/OR--- Execute the SG vreation CODE-BLOCK i.e. this code's PARENT .PY file
                                     #response.group_id(<FAILED) 'SGforInstanceIMR'(<FAILED)  #'SGforInstanceIMR'(<NEVER USE NAME EVEN A CALL USING NAME OLD ALREADY CREATED ONE FAILED)
            ],                       #https://gist.github.com/nguyendv/8cfd92fc8ed32ebb78e366f44c2daea6
            'SubnetId': 'subnet-0abd08a11c2f5fd58',          #????????????? "NetworkInterfaces=[]" and an "outer-block/instance-level 'Subnet ID'" SHOULD NOT be specified at same request
        },
    ],                  #-------------------------------------------------------------------------------------
)


#VERIFICATION OF ASSIGNMENT SUCCESS
#====================================
# aws configure set region us-east-2     #run this coomand in SSH Shell of spawned instance.
# aws dynamodb list-tables               #run this coomand in SSH Shell of spawned instance.

#aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId, IamInstanceProfile.Arn]' --output text
#aws ec2 describe-instances --query "Reservations[*].Instances[*].[InstanceId, IamInstanceProfile.Arn]" --output text      #BETTER one
#aws ec2 describe-instances --query "Reservations[*].Instances[*]" --output text                                           #GOOD
#aws ec2 describe-instances --query "Reservations[*].Instances[*]"                                                         #GOOD


#NOTE Knowledge Base
#==================
# SG MISC:
# A collection of SecurityGroup resources
# all()
# Creates an iterable of all SecurityGroup resources in the collection.
# See also: AWS API Documentation
# Request Syntax
# security_group_iterator = vpc.security_groups.all() !!!! IMR
# Return type
# list(ec2.SecurityGroup)
# Returns
# A list of SecurityGroup resources



#Questions:
#==========
#Why Assignment8 sample-code used SESSION (not client, resource) -- "session = boto3.session.Session(profile_name='default')"
#I used ENV as default in Assignment8 why what is DEFAULT and where is DEV, PROD to SET???????
#how to FUCKIN disable PY files accidental ENTER-press executions from Windows Explorer
#How we searched KEY name
#The deficiency in Assignment8 where the code 
#https://www.youtube.com/watch?v=VluBGIcr6so
#https://www.youtube.com/watch?v=LaW4MBwGhF0
#https://www.youtube.com/watch?v=I_Bk9iDNbUo
