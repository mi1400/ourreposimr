import boto3
import botocore
###### aws configure get region
###### aws configure set region us-east-2
from botocore.exceptions import ClientError


# aws sts get-caller-identity
# aws configure get region
# aws configure set region us-east-2
# client = boto3.client('ec2',region_name='us-east-2')
# response = client.describe_instances()
# print(response)

# # Boto 2.x
# import boto
# ec2_connection = boto.connect_ec2()
# vpc_connection = boto.connect_vpc()

#==================================================================================================================================================================
#GOOGLE: aws boto create SG
#https://boto3.amazonaws.com/v1/documentation/api/latest/guide/ec2-example-security-group.html
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.create_security_group       from above URL
#https://github.com/apache/kafka/pull/4878/files            find:response.groups().get(groupId)
#create_security_group()
#authorize_security_group_egress()
#authorize_security_group_ingress()
#update_security_group_rule_descriptions_egress()
#update_security_group_rule_descriptions_ingress()

#Exception Handling !!!!!!!
#https://boto3.amazonaws.com/v1/documentation/api/latest/guide/ec2-example-security-group.html          find:authorize_security_group_ingress

#OLD/LEGACY WAY----------------
#==============================
# client = boto3.client('elbv2',region_name='us-east-2')
# response = client.create_security_group(
#     Description='My new SG for Instance to access from home',
#     GroupName='SGforInstanceIMR',
#     VpcId='vpc-a360a8c8',
#     #DryRun=True|False
# )
# print(response)
# v_SG_ID = response['GroupId']
# #--------------use TRY CATCH next on.
#
# response = client.authorize_security_group_ingress(
#     GroupId='sg-903004f8',
#     IpPermissions=[
#         {
#             'FromPort': 22,
#             'IpProtocol': 'tcp',
#             'IpRanges': [
#                 {
#                     'CidrIp': '203.0.113.0/24',
#                     'Description': 'SSH access from the LA office',
#                 },
#             ],
#             'ToPort': 22,
#         },
#     ],
# )
# print(response)


#NEW/EXCEPTION HANDLING WAY----
#==============================
ec2 = boto3.client('ec2')

response = ec2.describe_vpcs()
v_vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', 'vpc-a360a8c8')           #???????????? should have called by NAME .. e.g. PRODUCTION_VPN
#v_security_group_id = None

try:
    response = ec2.create_security_group(GroupName='SGforInstanceIMR',
                                         Description='My new SG for Instance to access from home',
                                         VpcId=v_vpc_id)                                                #obselete: Tried ... "Name='SGforInstanceIMR')" #Unknown parameter in input: "Name", must be one of: Description, GroupName, VpcId, DryRun
    v_security_group_id = response['GroupId']
    print('Security Group Created %s in vpc %s.' % (v_security_group_id, v_vpc_id))


    #NO NEED "0.0.0.0/0" is always DEFAULT                #An error occurred (InvalidPermission.Duplicate) when calling the AuthorizeSecurityGroupEgress operation: the specified rule "peer: 0.0.0.0/0, ALL, ALLOW" already exists
    # data1 = ec2.authorize_security_group_egress(        #https://github.com/spulec/moto/issues/1791
    #     GroupId=v_security_group_id,
    #     IpPermissions=[
    #         {
    #             "IpProtocol": "-1",
    #             "IpRanges": [
    #                 {
    #                     "CidrIp": "0.0.0.0/0"
    #                 }
    #             ],
    #             "Ipv6Ranges": [],
    #             "PrefixListIds": [],
    #             "UserIdGroupPairs": []
    #         }
    #     ]
    # )
    # print('Egress Successfully Set %s' % data1)

    # import urllib2            #malware
    # def get_public_ip():
    # ext_ip = urllib2.urlopen("http://curlmyip.com").read()
    # return ext_ip.strip()

    data2 = ec2.authorize_security_group_ingress(
        GroupId=v_security_group_id,
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 80,
             'ToPort': 80,
             'IpRanges': [{'CidrIp': '70.31.63.90/32'}]}, #70.31.63.90/32        0.0.0.0/0          ????? How implement MY-IP
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '70.31.63.90/32'}]} #70.31.63.90/32         0.0.0.0/0          ????? How implement MY-IP
        ])
    print('Ingress Successfully Set %s' % data2)
except ClientError as e:
    print(e)
#https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/authorizing-access-to-an-instance.html             CLI


#==================================================================================================================================================================
#AWS DOCS: aws boto create instance
#https://boto3.amazonaws.com/v1/documentation/api/latest/guide/migrationec2.html
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.ServiceResource.create_instances ??????????
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Subnet.create_instances          ??????????
#RESEARCH:---------
#https://stackoverflow.com/questions/34028219/how-to-execute-commands-on-aws-instance-using-boto3
#GOOGLE:    "NetworkInterfaces" "create_instances" "create_security_group"
#GOOGLE:    "create_instances" "NetworkInterfaces" "create_security_group"
#https://stackoverflow.com/questions/48690956/using-boto3-how-can-i-associate-a-vpc-with-ec2-instance
#https://blog.ipswitch.com/how-to-create-and-configure-an-aws-vpc-with-python
#https://gist.github.com/nguyendv/8cfd92fc8ed32ebb78e366f44c2daea6                                              README IMR!!!
#https://github.com/miztiik/AWS-Demos/tree/master/How-To/setup-wordpress-in-ec2-in-5-minutes-with-boto          README IMR!!!   "globalVars['EC2-InstanceType']", "NetworkInterfaces=["


v_userData = """
                Content-Type: multipart/mixed; boundary="//"
                MIME-Version: 1.0

                --//
                Content-Type: text/cloud-config; charset="us-ascii"
                MIME-Version: 1.0
                Content-Transfer-Encoding: 7bit
                Content-Disposition: attachment; filename="cloud-config.txt"

                #cloud-config
                cloud_final_modules:
                - [scripts-user, always]

                --//
                Content-Type: text/x-shellscript; charset="us-ascii"
                MIME-Version: 1.0
                Content-Transfer-Encoding: 7bit
                Content-Disposition: attachment; filename="userdata.txt"

                #!/bin/bash
                /bin/echo "Hello World" >> /tmp/testfile.txt
                --//
             """

v_userData = """
                #!/bin/bash
                sudo yum update -y
                sudo yum install -y httpd
                sudo echo '<h1>Hello World</h1>' > /var/www/html/index.html
                sudo systemctl start httpd
                sudo systemctl enable httpd
             """

#https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html#user-data-shell-scripts
#https://aws.amazon.com/premiumsupport/knowledge-center/execute-user-data-ec2/
#https://www.bogotobogo.com/DevOps/AWS/aws-CloudFormation-Instance-Metadata-User-Data.php
#https://www.bogotobogo.com/DevOps/Terraform/Terraform-terraform-userdata.php               BETTER
v_userData = """
                #!/bin/bash
                sudo yum update -y
                sudo yum install -y httpd
                echo "<h1>Hello World</h1>" | sudo tee /var/www/html/index.html
                sudo systemctl start httpd
                sudo systemctl enable httpd
             """

# import boto3
# ec2 = boto3.resource('ec2')
# classic_address = ec2.ClassicAddress('public_ip')

#ec2 = boto3.resource('ec2',region_name='us-east-2')
#ec2 = boto3.resource('ec2')                            #ALREADY CREATED/INITIATED
ec2b = boto3.resource('ec2')
instance = ec2b.create_instances(
    # BlockDeviceMappings=[
    #     {
    #         'DeviceName': 'string',
    #         'VirtualName': 'string',
    #         'Ebs': {
    #             'DeleteOnTermination': True|False,
    #             'Iops': 123,
    #             'SnapshotId': 'string',
    #             'VolumeSize': 123,
    #             'VolumeType': 'standard'|'io1'|'gp2'|'sc1'|'st1',
    #             'KmsKeyId': 'string',
    #             'Encrypted': True|False
    #         },
    #         'NoDevice': 'string'
    #     },
    # ],
    ImageId='ami-0f7919c33c90f5b58', #IMR
    InstanceType='t2.micro', #'t1.micro'|'t2.nano'|'t2.micro'|'t2.small'|'t2.medium'|'t2.large'|'t2.xlarge'|'t2.2xlarge'|'t3.nano'|'t3.micro'|'t3.small'|'t3.medium'|'t3.large'|'t3.xlarge'|'t3.2xlarge'|'t3a.nano'|'t3a.micro'|'t3a.small'|'t3a.medium'|'t3a.large'|'t3a.xlarge'|'t3a.2xlarge'|'m1.small'|'m1.medium'|'m1.large'|'m1.xlarge'|'m3.medium'|'m3.large'|'m3.xlarge'|'m3.2xlarge'|'m4.large'|'m4.xlarge'|'m4.2xlarge'|'m4.4xlarge'|'m4.10xlarge'|'m4.16xlarge'|'m2.xlarge'|'m2.2xlarge'|'m2.4xlarge'|'cr1.8xlarge'|'r3.large'|'r3.xlarge'|'r3.2xlarge'|'r3.4xlarge'|'r3.8xlarge'|'r4.large'|'r4.xlarge'|'r4.2xlarge'|'r4.4xlarge'|'r4.8xlarge'|'r4.16xlarge'|'r5.large'|'r5.xlarge'|'r5.2xlarge'|'r5.4xlarge'|'r5.8xlarge'|'r5.12xlarge'|'r5.16xlarge'|'r5.24xlarge'|'r5.metal'|'r5a.large'|'r5a.xlarge'|'r5a.2xlarge'|'r5a.4xlarge'|'r5a.8xlarge'|'r5a.12xlarge'|'r5a.16xlarge'|'r5a.24xlarge'|'r5d.large'|'r5d.xlarge'|'r5d.2xlarge'|'r5d.4xlarge'|'r5d.8xlarge'|'r5d.12xlarge'|'r5d.16xlarge'|'r5d.24xlarge'|'r5d.metal'|'r5ad.large'|'r5ad.xlarge'|'r5ad.2xlarge'|'r5ad.4xlarge'|'r5ad.8xlarge'|'r5ad.12xlarge'|'r5ad.16xlarge'|'r5ad.24xlarge'|'x1.16xlarge'|'x1.32xlarge'|'x1e.xlarge'|'x1e.2xlarge'|'x1e.4xlarge'|'x1e.8xlarge'|'x1e.16xlarge'|'x1e.32xlarge'|'i2.xlarge'|'i2.2xlarge'|'i2.4xlarge'|'i2.8xlarge'|'i3.large'|'i3.xlarge'|'i3.2xlarge'|'i3.4xlarge'|'i3.8xlarge'|'i3.16xlarge'|'i3.metal'|'i3en.large'|'i3en.xlarge'|'i3en.2xlarge'|'i3en.3xlarge'|'i3en.6xlarge'|'i3en.12xlarge'|'i3en.24xlarge'|'i3en.metal'|'hi1.4xlarge'|'hs1.8xlarge'|'c1.medium'|'c1.xlarge'|'c3.large'|'c3.xlarge'|'c3.2xlarge'|'c3.4xlarge'|'c3.8xlarge'|'c4.large'|'c4.xlarge'|'c4.2xlarge'|'c4.4xlarge'|'c4.8xlarge'|'c5.large'|'c5.xlarge'|'c5.2xlarge'|'c5.4xlarge'|'c5.9xlarge'|'c5.12xlarge'|'c5.18xlarge'|'c5.24xlarge'|'c5.metal'|'c5d.large'|'c5d.xlarge'|'c5d.2xlarge'|'c5d.4xlarge'|'c5d.9xlarge'|'c5d.12xlarge'|'c5d.18xlarge'|'c5d.24xlarge'|'c5d.metal'|'c5n.large'|'c5n.xlarge'|'c5n.2xlarge'|'c5n.4xlarge'|'c5n.9xlarge'|'c5n.18xlarge'|'cc1.4xlarge'|'cc2.8xlarge'|'g2.2xlarge'|'g2.8xlarge'|'g3.4xlarge'|'g3.8xlarge'|'g3.16xlarge'|'g3s.xlarge'|'g4dn.xlarge'|'g4dn.2xlarge'|'g4dn.4xlarge'|'g4dn.8xlarge'|'g4dn.12xlarge'|'g4dn.16xlarge'|'cg1.4xlarge'|'p2.xlarge'|'p2.8xlarge'|'p2.16xlarge'|'p3.2xlarge'|'p3.8xlarge'|'p3.16xlarge'|'p3dn.24xlarge'|'d2.xlarge'|'d2.2xlarge'|'d2.4xlarge'|'d2.8xlarge'|'f1.2xlarge'|'f1.4xlarge'|'f1.16xlarge'|'m5.large'|'m5.xlarge'|'m5.2xlarge'|'m5.4xlarge'|'m5.8xlarge'|'m5.12xlarge'|'m5.16xlarge'|'m5.24xlarge'|'m5.metal'|'m5a.large'|'m5a.xlarge'|'m5a.2xlarge'|'m5a.4xlarge'|'m5a.8xlarge'|'m5a.12xlarge'|'m5a.16xlarge'|'m5a.24xlarge'|'m5d.large'|'m5d.xlarge'|'m5d.2xlarge'|'m5d.4xlarge'|'m5d.8xlarge'|'m5d.12xlarge'|'m5d.16xlarge'|'m5d.24xlarge'|'m5d.metal'|'m5ad.large'|'m5ad.xlarge'|'m5ad.2xlarge'|'m5ad.4xlarge'|'m5ad.8xlarge'|'m5ad.12xlarge'|'m5ad.16xlarge'|'m5ad.24xlarge'|'h1.2xlarge'|'h1.4xlarge'|'h1.8xlarge'|'h1.16xlarge'|'z1d.large'|'z1d.xlarge'|'z1d.2xlarge'|'z1d.3xlarge'|'z1d.6xlarge'|'z1d.12xlarge'|'z1d.metal'|'u-6tb1.metal'|'u-9tb1.metal'|'u-12tb1.metal'|'u-18tb1.metal'|'u-24tb1.metal'|'a1.medium'|'a1.large'|'a1.xlarge'|'a1.2xlarge'|'a1.4xlarge'|'a1.metal'|'m5dn.large'|'m5dn.xlarge'|'m5dn.2xlarge'|'m5dn.4xlarge'|'m5dn.8xlarge'|'m5dn.12xlarge'|'m5dn.16xlarge'|'m5dn.24xlarge'|'m5n.large'|'m5n.xlarge'|'m5n.2xlarge'|'m5n.4xlarge'|'m5n.8xlarge'|'m5n.12xlarge'|'m5n.16xlarge'|'m5n.24xlarge'|'r5dn.large'|'r5dn.xlarge'|'r5dn.2xlarge'|'r5dn.4xlarge'|'r5dn.8xlarge'|'r5dn.12xlarge'|'r5dn.16xlarge'|'r5dn.24xlarge'|'r5n.large'|'r5n.xlarge'|'r5n.2xlarge'|'r5n.4xlarge'|'r5n.8xlarge'|'r5n.12xlarge'|'r5n.16xlarge'|'r5n.24xlarge'|'inf1.xlarge'|'inf1.2xlarge'|'inf1.6xlarge'|'inf1.24xlarge',
    #Ipv6AddressCount=123,
    # Ipv6Addresses=[
    #     {
    #         'Ipv6Address': 'string'
    #     },
    # ],
    # KernelId='string',
    KeyName='Key_Test2_IMR',
    MaxCount=1, #123,
    MinCount=1, #123,
    # Monitoring={
    #     'Enabled': True|False
    # },
    # Placement={
    #     'AvailabilityZone': 'string',
    #     'Affinity': 'string',
    #     'GroupName': 'string',
    #     'PartitionNumber': 123,
    #     'HostId': 'string',
    #     'Tenancy': 'default'|'dedicated'|'host',
    #     'SpreadDomain': 'string',
    #     'HostResourceGroupArn': 'string'
    # },
    # RamdiskId='string',
    # SecurityGroups=[                                        # Network interfaces and an instance-level security groups may not be specified on the same request
    #      'SGforInstanceIMR', #'string',
    #  ],
    # SubnetId='subnet-0abd08a11c2f5fd58', #'string',         # Network interfaces and an instance-level subnet ID may not be specified on the same request
    UserData=v_userData, #'string',
    # AdditionalInfo='string',
    # ClientToken='string',
    DisableApiTermination=False, #True|False,                 # ?????????
    # DryRun=True|False,
    # EbsOptimized=True|False,
    # IamInstanceProfile={
    #     'Arn': 'string',                                    #will become Assignment9 giving here... find and copy ARN from IAM console >>> "arn:aws:iam::832922473570:role/ec2_DDB_Role_IMR_08" (<ARN from Assignment8 ie. last ROLE i created through this code) ### Can remove this through EC2 > Instance > Rightclick > Edit Profile something (around TERMINATE menue options/rightclick-popup)
    #     'Name': 'string'
    # },
    InstanceInitiatedShutdownBehavior='stop', #'stop'|'terminate',
    NetworkInterfaces=[ #-------------------------------------------------------------------------------------
        {
            'AssociatePublicIpAddress': True, #True|False,
            'DeleteOnTermination': True, #True|False,
            'Description': 'Boto Created Instance IMR',      #?????????????
            'DeviceIndex': 0, #123,                                             # The associatePublicIPAddress parameter can only be specified for the network interface with DeviceIndex 0.
            'Groups': [                                                         # GroupId (string) -- The ID of the security group. You must specify either the security group ID or the security group name in the request. For security groups in a nondefault VPC, you must specify the security group ID.          Groups == SecurityGroups -- IMR
                 response['GroupId']#(<WORKS) #v_security_group_id#(<WORKS)     # The security group 'sgforinstanceimr' does not exist in VPC 'vpc-a360a8c8' ..... MUST SET REGION ---AND/OR--- Execute the SG vreation CODE-BLOCK i.e. this code's PARENT .PY file
                                    #response.group_id(<FAILED) #'SGforInstanceIMR'(<NEVER USE NAME EVEN A CALL USING NAME OLD ALREADY CREATED ONE FAILED)
            ],                      #https://gist.github.com/nguyendv/8cfd92fc8ed32ebb78e366f44c2daea6
            # 'Ipv6AddressCount': 123,
            # 'Ipv6Addresses': [
            #     {
            #         'Ipv6Address': 'string'
            #     },
            # ],
            # 'NetworkInterfaceId': 'string',
            # 'PrivateIpAddress': 'string',                  #?????????????
            # 'PrivateIpAddresses': [
            #     {
            #         'Primary': True|False,
            #         'PrivateIpAddress': 'string'
            #     },
            # ],
            # 'SecondaryPrivateIpAddressCount': 123,
            'SubnetId': 'subnet-0abd08a11c2f5fd58',          #????????????? "NetworkInterfaces=[]" and an "outer-block/instance-level 'Subnet ID'" SHOULD NOT be specified at same request
            # 'InterfaceType': 'string'
        },
    ],                  #-------------------------------------------------------------------------------------
    # PrivateIpAddress='string',
    # ElasticGpuSpecification=[
    #     {
    #         'Type': 'string'
    #     },
    # ],
    # ElasticInferenceAccelerators=[
    #     {
    #         'Type': 'string',
    #         'Count': 123
    #     },
    # ],
    # TagSpecifications=[
    #     {
    #         'ResourceType': 'client-vpn-endpoint'|'customer-gateway'|'dedicated-host'|'dhcp-options'|'elastic-ip'|'fleet'|'fpga-image'|'host-reservation'|'image'|'instance'|'internet-gateway'|'key-pair'|'launch-template'|'natgateway'|'network-acl'|'network-interface'|'placement-group'|'reserved-instances'|'route-table'|'security-group'|'snapshot'|'spot-fleet-request'|'spot-instances-request'|'subnet'|'traffic-mirror-filter'|'traffic-mirror-session'|'traffic-mirror-target'|'transit-gateway'|'transit-gateway-attachment'|'transit-gateway-multicast-domain'|'transit-gateway-route-table'|'volume'|'vpc'|'vpc-peering-connection'|'vpn-connection'|'vpn-gateway'|'vpc-flow-log',
    #         'Tags': [
    #             {
    #                 'Key': 'string',
    #                 'Value': 'string'
    #             },
    #         ]
    #     },
    # ],
    # LaunchTemplate={
    #     'LaunchTemplateId': 'string',
    #     'LaunchTemplateName': 'string',
    #     'Version': 'string'
    # },
    # InstanceMarketOptions={
    #     'MarketType': 'spot',
    #     'SpotOptions': {
    #         'MaxPrice': 'string',
    #         'SpotInstanceType': 'one-time'|'persistent',
    #         'BlockDurationMinutes': 123,
    #         'ValidUntil': datetime(2015, 1, 1),
    #         'InstanceInterruptionBehavior': 'hibernate'|'stop'|'terminate'
    #     }
    # },
    # CreditSpecification={
    #     'CpuCredits': 'string'
    # },
    # CpuOptions={
    #     'CoreCount': 123,
    #     'ThreadsPerCore': 123
    # },
    # CapacityReservationSpecification={
    #     'CapacityReservationPreference': 'open'|'none',
    #     'CapacityReservationTarget': {
    #         'CapacityReservationId': 'string'
    #     }
    # },
    # HibernationOptions={
    #     'Configured': True|False
    # },
    # LicenseSpecifications=[
    #     {
    #         'LicenseConfigurationArn': 'string'
    #     },
    # ],
    # MetadataOptions={
    #     'HttpTokens': 'optional'|'required',
    #     'HttpPutResponseHopLimit': 123,
    #     'HttpEndpoint': 'disabled'|'enabled'
    # }
)



#SUCCESS!!!!!!!
#==================
# C:\WORK\PERSONAL\AWS\Python>"C:/Program Files/Python37/python.exe" c:/WORK/PERSONAL/AWS/Python/awsPyNBotoAssign7.py
# Security Group Created sg-0ccb4e6659db1364d in vpc vpc-a360a8c8.
# Ingress Successfully Set {'ResponseMetadata': {'RequestId': 'a5beb1db-9d5b-4f1b-88f2-fccd3fed7da3', 'HTTPStatusCode': 200, 'HTTPHeaders': {'x-amzn-requestid': 'a5beb1db-9d5b-4f1b-88f2-fccd3fed7da3', 'content-type': 'text/xml;charset=UTF-8', 'content-length': '259', 'date': 'Mon, 20 Apr 2020 21:21:54 GMT', 'server': 'AmazonEC2'}, 'RetryAttempts': 0}}
# C:\WORK\PERSONAL\AWS\Python>


#Knowledge Base
#==================
# SG MISC:
# A collection of SecurityGroup resources
# all()
# Creates an iterable of all SecurityGroup resources in the collection.
# See also: AWS API Documentation
# Request Syntax
# security_group_iterator = vpc.security_groups.all() !!!! IMR
# Return type
# list(ec2.SecurityGroup)
# Returns
# A list of SecurityGroup resources


#Questions:
#==========
#How we searched KEY name
#https://www.youtube.com/watch?v=VluBGIcr6so
#https://www.youtube.com/watch?v=LaW4MBwGhF0
#https://www.youtube.com/watch?v=I_Bk9iDNbUo

#Next Assignment (del all prev)
#make SG, all traffic from home (any port)
#instance typical should pass through above SG (boot strapping***)
#