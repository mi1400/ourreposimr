import boto3
import botocore
###### aws configure get region
###### aws configure set region us-east-2
from botocore.exceptions import ClientError
import json

# aws sts get-caller-identity
# aws configure get region
# aws configure set region us-east-2
# client = boto3.client('ec2',region_name='us-east-2')
# response = client.describe_instances()
# print(response)
# DryRun=True|False

#==================================================================================================================================================================
#GOOGLE: aws boto create dynamodb
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html
#https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.html
#   https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.01.html
#   https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.03.html


# dynamodb = boto3.resource('dynamodb', region_name='us-east-2') #, endpoint_url="http://localhost:8000")
# table = dynamodb.create_table(
#     TableName='DDB_Movies_IMR',
#     KeySchema=[
#         {
#             'AttributeName': 'year',
#             'KeyType': 'HASH'  #Partition key
#         },
#         {
#             'AttributeName': 'title',
#             'KeyType': 'RANGE'  #Sort key
#         }
#     ],
#     AttributeDefinitions=[
#         {
#             'AttributeName': 'year',
#             'AttributeType': 'N'
#         },
#         {
#             'AttributeName': 'title',
#             'AttributeType': 'S'
#         },

#     ],
#     ProvisionedThroughput={
#         'ReadCapacityUnits': 10,
#         'WriteCapacityUnits': 10
#     }
# )

# print("Table status:", table.table_status)

#=OBSELETE=================================================================================================================================================================
#GOOGLE: "boto3.resource('iam')" "iam.create_role"                  "<URL-encoded-JSON>"
#https://www.programcreek.com/python/example/97939/boto3.session
#https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3

# # Connect to IAM with boto
# #iam = boto3.connect_iam($key, $secret)
#
# # Create IAM client
# iam = boto3.client('iam')
#
# #createRole
# S3ANDEC2 = {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "S3ReadOnly",
#             "Effect": "Allow",
#             "Action": [
#                 "s3:Get*",
#                 "s3:List*"
#             ],
#             "Resource": [
#                 "*"
#             ]
#         },
#         {
#             "Sid": "Ec2FullAccess",
#             "Action": "ec2:*",
#             "Effect": "Allow",
#             "Resource": "*"
#         }
#     ]
# }
#
# response = iam.create_role(
#     Path='/',
#     RoleName='EC2_DDB_Profile_IMR', #'Boto-R1',
#     AssumeRolePolicyDocument=json.dumps(S3ANDEC2),
#     Description='S3 Read and EC2Full permissions policy'
# )
#
# print(response)
#---------------------------------------------------------------------------------------------------

#======================================================================================================================================================
#======================================================================================================================================================
#https://www.programcreek.com/python/example/97939/boto3.session
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html
#https://aws.amazon.com/blogs/security/how-to-create-an-aws-iam-policy-to-grant-aws-lambda-access-to-an-amazon-dynamodb-table/      README IMR
#https://www.reddit.com/r/aws/comments/4zq5sh/help_with_elastic_beanstalk_dynamodb_tutorial/                                        README IMR
#https://boto3.amazonaws.com/v1/documentation/api/latest/guide/iam-examples.html
#https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_dynamodb_specific-table.html                          AWS Identity and Access Management -- USER GUIDE -- MSDN type
#https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/using-identity-based-policies.html                                
#https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3                         README IMR
      #You can't attach policy to a role by using AssumeRolePolicyDocument, it is used to attach a trust policy to the role.
      #...you "create a role"<-->"attach trust policy" to it, "create a policy"<--> "attach policy to the role"

#print(boto3.session.Session().available_profiles)      #default        #use this to capture        https://stackoverflow.com/questions/33378422/how-to-choose-an-aws-profile-when-using-boto3-to-connect-to-cloudfront
session = boto3.session.Session(profile_name='default')
iam = session.client('iam')
path='/'
role_name='ec2_DDB_Role_IMR_11'           #also called "Instance Profile"
description='BOTO3 ec2 test role IMR'

trust_policy={
  "Version": "2012-10-17",
  "Statement": [
    {
      #"Sid": "",                           #???????????? StatementIDs (SID)
      "Effect": "Allow",
      "Principal": {                        #Principal – In identity-based policies (IAM policies), the user that the policy is attached to is the implicit principal. For resource-based policies, you specify the user, account, service, or other entity that you want to receive permissions (applies to resource-based policies only). DynamoDB doesn't support resource-based policies.
                                            #tell AWS that EC2 is allowed to use this role by listing the EC2 service in the role's trust policy (they're being very polite about this; they'd never just assume that you wanted EC2 to access a role without your explicit say so.)    ||| Go into IAM > Policies > Create Policy > Policy Generator. Build your policy there and you can't go wrong. ||| https://www.reddit.com/r/aws/comments/4zq5sh/help_with_elastic_beanstalk_dynamodb_tutorial/
        "Service": "ec2.amazonaws.com"      #not usable by lambda in this scenario
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
#^^^^^^^^^^^^^vvvvvvvvvvvvvvvvv
response = iam.create_role(                 #also called "Instance Profile"
    Path=path,
    RoleName=role_name,
    AssumeRolePolicyDocument=json.dumps(trust_policy),    #https://forums.aws.amazon.com/thread.jspa?messageID=831798&tstart=0        #https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3
    Description=description,
    MaxSessionDuration=3600 #<seconds -- 3600(1hr) by default   #1800 WRONG IMR -- Invalid range for parameter MaxSessionDuration, value: 1800, valid range: 3600-inf
)

#======================================================================================================================================================
#dynamodb managed policy
#https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/using-identity-based-policies.html
#https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_dynamodb_specific-table.html

managed_policy = {                  #original: https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3
    "Version": "2012-10-17",
    "Statement": [
        # {
        #     "Sid": "ListAndDescribe", #"S3ReadOnly",
        #     "Effect": "Deny", #"Allow"                            #Changing to DENY to make sense in 2 blocks
        #     "Action": [
        #         "dynamodb:List*", #"s3:Get*",
        #         "dynamodb:DescribeReservedCapacity*", #"s3:List*"
        #         "dynamodb:DescribeLimits",
        #         "dynamodb:DescribeTimeToLive"
        #     ],
        #     "Resource": [
        #         "*"
        #     ]
        # },
        {
            "Sid": "Ec2FullAccess", #"Ec2FullAccess", #SpecificTable #"DDB_Movies_IMR"
            "Effect": "Allow",
            "Action": "dynamodb:*", #https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_dynamodb_specific-table.html
            "Resource": "arn:aws:dynamodb:us-east-2:832922473570:table/DDB_Movies_IMR"      #   "*" also WORK!!!        #"arn:aws:dynamodb:us-west-2:123456789012:table/Books"      #The ARN value specified in the Resource identifies a table in a specific Region.
        }
    ]
}
#^^^^^^^^^^^^^vvvvvvvvvvvvvvvvv
response = iam.create_policy(
  PolicyName='testEc2PolicyIMR_11', #???? Significance Vs Administrator         #BOTO3TestEc2PolicyIMR         #botocore.errorfactory.MalformedPolicyDocumentException: An error occurred (MalformedPolicyDocument) when calling the CreatePolicy operation: Statement IDs (SID) must be alpha-numeric. Check that your input satisfies the regular expression [0-9A-Za-z]*
  PolicyDocument=json.dumps(managed_policy)
)

#======================================================================================================================================================
iam.attach_role_policy(
    #PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess'                    #https://www.programcreek.com/python/example/97939/boto3.session
    PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess',                    #'arn:aws:iam::${account_id}:policy/testEc2PolicyIMR' #https://stackoverflow.com/questions/45184780/trying-to-create-iam-policy-role-and-users-using-python-boto3
                                                                                #VSCODE-ERROR: botocore.errorfactory.InvalidInputException: An error occurred (InvalidInput) when calling the AttachRolePolicy operation: ARN arn:aws:iam::${account_id}:policy/testEc2PolicyIMR is not valid.
    RoleName='ec2_DDB_Role_IMR_11'  #Better use role_name custom variable
)
#======================================================================================================================================================
#======================================================================================================================================================

#NOT OVER .. FINAL STEP... ELSE "Instance Profile ARNs" will remain MISSING/NULL in IAM > ROLE > 3rd-ROW "Instance Profile ARNs"
# IT has remained NULL for all except now i fixed with CLI code at bottom of this section
#ec2_DDB_Role_IMR
#ec2_DDB_Role_IMR_02
#ec2_DDB_Role_IMR_03
#ec2_DDB_Role_IMR_04
#ec2_DDB_Role_IMR_05
#ec2_DDB_Role_IMR_06
#ec2_DDB_Role_IMR_07
#ec2_DDB_Role_IMR_08      <<<<<<<<<<<<<<< FIXED
#ec2_DDB_Role_IMR_10
#ec2_DDB_Role_IMR_11

#********** FIXER ***********
#CLI way
#========
# aws iam create-instance-profile --instance-profile-name MyExistingRole
# aws iam add-role-to-instance-profile --instance-profile-name MyExistingRole --role-name MyExistingRole

# aws iam create-instance-profile --instance-profile-name ec2_DDB_Role_IMR_08
# aws iam add-role-to-instance-profile --instance-profile-name ec2_DDB_Role_IMR_08 --role-name ec2_DDB_Role_IMR_08

# aws iam create-instance-profile --instance-profile-name ec2_DDB_Role_IMR_11                                         #https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.create_instance_profile
# aws iam add-role-to-instance-profile --instance-profile-name ec2_DDB_Role_IMR_11 --role-name ec2_DDB_Role_IMR_11    #https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.add_role_to_instance_profile

#Boto way
#========
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.create_instance_profile
response = client.create_instance_profile(
    InstanceProfileName='ec2_DDB_Role_IMR_10',    #'Webserver',
)
print(response)

#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.add_role_to_instance_profile
response = client.add_role_to_instance_profile(
    InstanceProfileName='ec2_DDB_Role_IMR_10',    #'Webserver',
    RoleName='ec2_DDB_Role_IMR_10',               #'S3Access',
)
print(response)

#======================================================================================================================================================
#======================================================================================================================================================


#Knowledge Base
#==================
#"Action": [ "dynamodb:PutItem" ], "Resource": [ "*" ]
#remove the [] from these lines and the other two as well.
#single items don't go in an array in JSON.
#---------------------
# SG MISC:
# A collection of SecurityGroup resources
# all()
# Creates an iterable of all SecurityGroup resources in the collection.
# See also: AWS API Documentation
# Request Syntax
        # security_group_iterator = vpc.security_groups.all() !!!! IMR
        # Return type
        # list(ec2.SecurityGroup)
# Returns
# A list of SecurityGroup resources
#---------------------


#Questions:
#==========
#Why this Assignment8 sample-code used SESSION (not client, resource) -- "session = boto3.session.Session(profile_name='default')""
#s3.meta.client.generate_presigned_url
#How we searched KEY name
#https://www.youtube.com/watch?v=VluBGIcr6so
#https://www.youtube.com/watch?v=LaW4MBwGhF0
#https://www.youtube.com/watch?v=I_Bk9iDNbUo
